package main

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gopkg.in/urfave/cli.v2"
	"kilabs/internal"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"
)

var (
	// AppName export name of service
	AppName = "Calendar-API"
	// AppVer export version of service
	AppVer = "1.0"
)

const (
	// TIMEOUT server connection timeout
	TIMEOUT = time.Second * 30
)

func init() {
	// sets the maximum number of CPUs that can be executing
	runtime.GOMAXPROCS(runtime.NumCPU())
}

func main() {
	app := cli.App{
		Name:        AppName,
		Version:     AppVer,
		Description: "Calendar api is an api to get all available slots between a Candidate and Interviewers",
		Action:      runService,
		Flags: []cli.Flag{
			&cli.IntFlag{Name: "port", Value: 8080, Usage: "listen port"},
			&cli.StringFlag{Name: "addr", Value: "0.0.0.0", Usage: "HTTP listen address"},
		},
	}

	app.Flags = append(app.Flags, []cli.Flag{}...)
	if len(os.Args) == 1 {
		os.Args = append(os.Args, app.Name)
	}
	err := app.Run(os.Args)
	if err != nil {
		os.Exit(1)
	}
	os.Exit(0)
}

func runService(cli *cli.Context) error {
	addr := fmt.Sprintf("%s:%d", cli.String("addr"), cli.Int("port"))

	// Set some default logging fields - no need to repeat them all over the place
	logger := log.New().WithFields(log.Fields{
		"service": AppName,
		"version": AppVer,
	})

	// Mux
	r := mux.NewRouter()

	// server
	s := &http.Server{
		Addr:           addr,
		Handler:        r,
		ReadTimeout:    TIMEOUT,
		WriteTimeout:   TIMEOUT,
		MaxHeaderBytes: 1 << 20,
	}

	// Graceful kill
	serverErrors := make(chan error, 1)
	// Start the listener.
	go func() {
		logger.WithFields(log.Fields{"when": date.Now(), "when_ts": date.NowUnix()}).Info("HTTP is running: ", addr)
		serverErrors <- s.ListenAndServe()
	}()

	// Make a channel to listen for an interrupt or terminate signal from the OS.
	// Use a buffered channel because the signal package requires it.
	osSignals := make(chan os.Signal, 1)
	signal.Notify(osSignals, os.Interrupt, syscall.SIGTERM)

	// Block waiting for a receive on either channel
	select {
	case err := <-serverErrors:
		logger.WithFields(log.Fields{"when": date.Now(), "when_ts": date.NowUnix()}).Fatalf("Error starting server: %v", err)

	case <-osSignals:
		// Create a context to attempt a graceful 5 second shutdown.
		ctx, cancel := context.WithTimeout(context.Background(), 5 * time.Second)
		defer cancel()
		// Attempt the graceful shutdown by closing the listener and
		// completing all inflight requests.
		if err := s.Shutdown(ctx); err != nil {
			logger.WithFields(log.Fields{"when": date.Now(), "when_ts": date.NowUnix()}).Infof("Could not stop server gracefully: %v", err)
			logger.WithFields(log.Fields{"when": date.Now(), "when_ts": date.NowUnix()}).Info("Initiating hard shutdown")
			if err := s.Close(); err != nil {
				logger.WithFields(log.Fields{"when": date.Now(), "when_ts": date.NowUnix()}).Fatalf("Could not stop http server: %v", err)
			}
		}
	}

	logger.WithFields(log.Fields{"when": date.Now(), "when_ts": date.NowUnix()}).Info("Shut down successful")

	return nil
}
