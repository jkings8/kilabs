# Run API inside Docker
## Build docker image
$ docker build -t kilabs .
## List all your images
$ docker image ls
## Run docker image
$ docker run -d -p 8080:8080 kilabs
## Ping 
$ curl http://localhost:8080

# Run API locally 
## Install dependencies
$ go mod tidy
## Run default configurations
$ go run main.go
## Run help command to see all possible configurations
$ go run main.go --help