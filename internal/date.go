package date

import "time"

const (
	TimeFormat = "2006-01-02T15:04:05.000Z07:00" // RFC 3339
)

func Now() string {
	return time.Now().UTC().Format(TimeFormat)
}

func NowUnix() int64 {
	return time.Now().UTC().UnixNano() / int64(time.Millisecond)
}
