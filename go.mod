module kilabs

go 1.12

require (
	github.com/gorilla/mux v1.7.3
	github.com/sirupsen/logrus v1.4.2
	gopkg.in/urfave/cli.v2 v2.0.0-20190806201727-b62605953717
)
